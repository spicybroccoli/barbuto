<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'db142268_barbuto');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'db142268');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'fingered!');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'internal-db.s142268.gridserver.com');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'a`M~GOpd>nSJ|B+6PqF-iajI|Ej&!f|<Oj&Ey1^tVrtnhw?xX7 WNS6aQy5N}]Um');
define('SECURE_AUTH_KEY',  'C|$*jR@V*At:G:>[5 y.nQ,N)Qplb2hLEM<kCLCIu)vM}O3C`tB48Pofu5k%]&0[');
define('LOGGED_IN_KEY',    'B}8I@CQW?k/F]FkuS6IZTCzu;Kbb@M7+~1bx5zYjE2(QH&0Wi?|]!{e_/Qi$F& a');
define('NONCE_KEY',        'O[s.)9k9+Z6]n3`Fm ch:@j&gi$zz+Ud?=gVMA,-WtUA`GP*6g^T~fX#Z+[-}dT+');
define('AUTH_SALT',        '5UCeYGBp}wJ2b9%sX24[J!g1T-P-b[N|X;p;C&/|I#|)|Awia_bQ$e5~5!4*u!^f');
define('SECURE_AUTH_SALT', 'kT+y(J.Fq|as_,5p#MY[]:(bOaoh}*+Ng8vM3;tSC:o2h9[d<|FfX2969:IM(pa%');
define('LOGGED_IN_SALT',   '3=S^}h+Kwxz@67HX#1`Ei(_jw3&5-T[c,?DmSfX_-8Wl#8$=0D5NVbvhA[$[Ar@i');
define('NONCE_SALT',       ':k53=YV.==lKT0vR><R9%y?HIr>|pIaX1`|TwOq!G:5:Q)oCV-lOk;h$)XySzv4]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sbm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
