<?php

/**

 * The Header for our theme.

 */

?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

<meta charset="<?php bloginfo( 'charset' ); ?>" />

<title><?php

	/*

	 * Print the <title> tag based on what is being viewed.

	 */

	global $page, $paged;



	wp_title( '|', true, 'right' );



	// Add the blog name.

	bloginfo( 'name' );



	// Add the blog description for the home/front page.

	$site_description = get_bloginfo( 'description', 'display' );

	if ( $site_description && ( is_home() || is_front_page() ) )

		echo " | $site_description";



	// Add a page number if necessary:

	if ( $paged >= 2 || $page >= 2 )

		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );



	?></title>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/style-responsive.css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Add fancyBox -->

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/fancybox/source/helpers/jquery.fancybox-media.js"></script>



<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/app.js"></script>



<?php wp_head(); ?>

</head>



<body <?php body_class(); ?>>



<header>

<a href="#" id="trigger" class="menu-trigger"></a>

<div class="logo-fixed">

        	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/barbuto_logo.jpg" alt="" class="img-responsive"/>

    	</div>

<div class="container">

<div class="header-content">

<div class="header-topo">

	<div class="social">
    	<ul>
        	<li class="facebook"><a href="https://www.facebook.com/barbutosydney" target="_blank"></a></li>
            <li class="twitter"><a href="https://twitter.com/barbutosydney" title="@BarbutoSydney" target="_blank"></a></li>
            <li class="gplus"><a href="#" target="_blank"></a></li>
        </ul>
    </div>

	<div class="phone"><span>call us now</span> (02) 9970 6171</div>

</div>

      <nav>

         <ul>

          <li>

            <a href="#about-us" class="about-us">

              <span>About Us</span>

            </a>

          </li>

          <li>

            <a href="#menus" class="menus">

              <span>Menus</span>

            </a>

          </li>

          <li>

            <a href="#whats-on" class="whats-on">

              <span>What's On</span>

            </a>

          </li>

          <li class="palmeira"></li>

          <li>

            <a href="#contact" class="contact">

              <span>contact</span>

            </a>

          </li>

          <li>

            <a href="#gallery" class="gallery">

              <span>gallery</span>

            </a>

          </li>

          <li>

            <a href="#catering" class="catering">

              <span>catering</span>

            </a>

          </li>

        </ul>

        </nav>

      </div>

      </div>

    </header><!-- HEADER -->