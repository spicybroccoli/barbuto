var exec = require('child_process').exec;
var needle = require('needle');
var slugify = require('slugify');
var path = require('path');
var fs = require('fs');

module.exports = function(grunt) {

	var app = {
		pluginList: ['all-in-one-wp-security-and-firewall','contact-form-7','all-in-one-seo-pack','iwp-client'],
		projectName: '',
		projectNameSlug: '',
		bitBucketSlug: '',
		path: ''
	};

	grunt.registerTask('setup', 'My "default" task description.', function() {
		grunt.task.run('getDir','createRemoteRepo', 'addRemoteRepo', 'commitPush', 'pluginSetup');
	});

	grunt.registerTask('getDir', 'My "default" task description.', function() {
 		
 		var done = this.async();

 		exec('git rev-parse --show-toplevel', function(e, stdout, stderr){
 			app.path = stdout;
 			app.projectName = path.basename(app.path);
			app.projectNameSlug = slugify(app.projectName);
			app.bitBucketSlug = slugify(app.projectName, '+');
			done();
 		});

	});

	grunt.registerTask('createRemoteRepo', 'createRemoteRepo', function() {
		var done = this.async();
		var username = "spicybroccoli",
		    password = "spicy",
		    url = "https://api.bitbucket.org/1.0/repositories",
		    params = "name="+app.bitBucketSlug,
		    auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

		var options = {
		  headers: { 'Authorization': auth }
		}

		needle.post(url, params, options, function(err, resp) {
		  done();
		});
	});

	grunt.registerTask('addRemoteRepo', 'Add Remote Repo', function(){
		var done = this.async();
		exec('git remote add origin https://spicybroccoli:spicy@bitbucket.org/spicybroccoli/' + app.projectNameSlug + '.git', function(error, stdout, stderr) {
		    if (error !== null) {
		        console.log('exec error: ' + error);
		    }
		    console.log('Git URL: ' + 'https://spicybroccoli:spicy@bitbucket.org/spicybroccoli/' + app.projectNameSlug + '.git');
		    done();
		});
	});

	grunt.registerTask('commitPush', 'Commit & Push', function(){
		var done = this.async();
		console.log('Adding files to stage....');
		exec('cd ' + app.path + ' && git add -A', function(e, stdout, stderr){
			console.log('Committing...');
			exec('git commit -am "Initial Commit"',function(e, stdout, stderr){
				console.log('Pushing to Origin....');
				exec('git push -u origin --all', function(e, stdout, stderr){
					done();
				});
			});
		})
	});

	grunt.registerTask('pluginSetup', 'Install Plugins', function(){
		var done = this.async();
		var plc = app.pluginList.length;
		for(i = 0; i < plc; i++) {
				(function(c) {
					var plugin = app.pluginList[c];
					console.log('Installing '+ plugin);
					exec('wp plugin install ' + plugin + ' --activate', function(e, stdout, stderr){
						if (c == plc) {
							done();
						}
					});
				})(i);
				
		}
	});

	grunt.registerTask('createDbfile', 'Create .dbname file', function(){
		var done = this.async();
		fs.readFile(app.path + '.yeopress', 'utf8', function (err, data) {
			if (err) {
			    console.log('Error: ' + err);
			    return;
			}
			 
			data = JSON.parse(data);
			console.log(data.dbName);
			fs.writeFile(app.path+".dbname", data.dbName, function(err) {
			    if(err) {
			        console.log(err);
			    } else {
			        console.log("The file was saved!");
			    }
			    done();
			}); 
		});
	});

}