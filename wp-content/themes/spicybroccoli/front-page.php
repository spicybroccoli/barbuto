<?php get_header(); ?>

<div id="main" class="clearfix">
  <section class="page" id="home">
    <div class="container">
      <div class="logo"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-transparent.png" alt="" class="img-responsive"/> </div>
    </div>
    <div class="home-footer">
      <div class="container">
        <div class="book-online"> <a class="btn-black-white" href="javascript:void(window.open('http://widget.dimmi.com.au/entry/26210?referrerUrl=http://www.barbutorestaurant.com.au/','Dimmi','width=302,height=432,top=60,left=60'))" target="_blank"> BOOK<br>
          ONLINE</a></div>
        <div class="badge"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/img_badge.png"  alt=""/> </div>
        <div class="wrapper">
          <div class="col-1">we are open lunches & dinners<br>
            7 days a week</div>
          <div class="col-2">RESERVATIONS<br>
            (02)9970 6171 </div>
        </div>
      </div>
    </div>
  </section>
  <!-- HOME -->
  <section class="page" id="about-us">
    <div class="vert-middle-align">
      <div class="container">
        <h1>
          <?php
// GET ONLY TITLE FORM ABOUT US PAGE ID #2
echo get_the_title(2);
?>
        </h1>
        <?php 
		//GET ALL CHILD PAGES FROM PARENT CATEGORY
		$args = array(
		'post_parent' => 2,'order' => 'ASC',
		'order_by' => 'menu_order'
		); 
		$children_about = get_children( $args, $output ); 
		foreach( $children_about as $post ) :
		?>
        <div class="row">
          <div class="two-col">
            <?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
            <img style="width:450px;" src="<?php echo $feat_image; ?>" class="img-responsive"> </div>
          <div class="two-col">
            <p> <?php echo $post->post_content; ?></p>
          </div>
        </div>
        <?php endforeach; ?>
      </div>
    </div>
  </section>
  <!-- ABOUT US -->
  <section class="page" id="menus">
    <div class="container"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/img_menu.jpg" width="765" height="543" class="img-responsive"/>
      <div class="book-now-abs"> <a target="_blank" href="http://www.dimmi.com.au/restaurant/barbuto" title="Book Now"></a> </div>
      <?php wp_nav_menu( array(
			'menu' => 'Menu PDFs', 
			'container' => false,
			'items_wrap'      => '<ul id="%1$s" class="menu-links">%3$s</ul>',
			)); ?>
    </div>
  </section>
  <!-- MENUS -->
  <section class="page" id="whats-on">
    <div class="container">
      <div class="row">
        <h1>
          <?php
				// GET ONLY TITLE FORM WHAT'S ON PAGE ID #13
				echo get_the_title(13);
				?>
						</h1>
						<?php 
				//GET ALL CHILD PAGES FROM PARENT CATEGORY
				$args = array(
					'post_parent' => 13,
					'order' => 'ASC',
					'order_by' => 'menu_order'
				); 
				$children_about = get_children( $args, $output ); 
				$col_count = 0; //col counter
				foreach( $children_about as $post ) :
					$col_count++;
				?>
        <div class="col-<?php echo $col_count; ?>"> <a class="box-inline" href="#inline-wo-<?php echo $col_count; ?>">
          <?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
          <img src="<?php echo $feat_image; ?>" class="img-center img-responsive"/><span><?php echo $post->post_title; ?></span> </a>
          <div id="inline-wo-<?php echo $col_count; ?>" style="display:none;">
            <div class="inline-title"><span><?php echo $post->post_title; ?></span></div>
            <div class="inline-content"><?php echo $post->post_content; ?></div>
          </div>
        </div>
        <?php endforeach; ?>
      </div>
            
      <?php 
	  $post = get_post(124); // UPCOMING EVENT PAGE ?>
      <?php 
	  if(get_post_status($post->ID) == 'publish'){ 
	  	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>
		<div class="upcoming-events">
            <div class="col-1"></div>
            <div class="col-2"><h4><?php echo $post->post_title; ?></h4></div>
            <div class="col-3">
            	<div class="event-img">
                <a class="box-inline" href="#inline-wo-upcoming-event">
                	<img src="<?php echo $feat_image; ?>"  class="img-responsive"/>
                </a>
                </div>
            </div>
            <div id="inline-wo-upcoming-event" style="display:none; background-image: url(<?php echo $feat_image; ?>)">
                <div class="inline-title"><span><?php echo $post->post_title; ?></span></div>
                <div class="inline-content"><?php echo $post->post_content; ?>
                	<?php $key_1_values = get_post_meta( get_the_ID(), 'pdf_url' );?>
                    <?php if($key_1_values){ ?>
                    	<span class="download-pdf"><a href="<?php echo $key_1_values[0]; ?>" target="_blank">See event's PDF</a></span>
                    <?php } ?>
                </div>
          	</div>
          </div>
          <?php } ?>
    <?php /*
      <div class="our-vip-club-bar">
        <p>find out all the latest specials and fun stuff that’s going on at Barbuto restaurant</p>
        <a class="btn-black-white vip-club" href="#">Our Vip Club</a> </div>
		*/ ?>
    </div>
  </section>
  <!-- WHATS ON -->
  <section class="page" id="contact">
    <div class="container">
      <div class="header">
        <h1>Contact Us</h1>
        <p>General and function enquiries, bookings, employment etc.</p>
      </div>
      <div class="contact-form"> <?php echo do_shortcode( '[contact-form-7 id="43" title="Contact form 1" html_class="use-floating-validation-tip"]' ) ?>
        <div class="col-3"> <a class="box-media" href="http://maps.google.com/maps?q=Barbuto+Restaurant,+Ocean+Street,+Paris,+France&t=h&z=17"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/img_contact_map.jpg" width="310" height="205"  alt="" class="img-responsive"/> </a>
        
        <div class="address"><span>(02) 9970 6171</span>
        <p>Shop 5/16 Ocean Street,
          Narrabeen, NSW 2101 </p>
          </div></div>
      </div>
    </div>
  </section>
  <!-- CONTACT -->
  <section class="page" id="gallery">
    <div class="container">
      <h1>Gallery</h1>
      <ul id="gallery_images">
        <?php  
/*
* GET ALL IMAGES URL FROM GALLERY PAGE (ID 27) TO BE SHOWED ON GALLERY  SECTION
* THE LOOP WILL SHOW ONLY THE FIRST 8 IMAGES
*/
$gallery = get_post_gallery_images( 27 ); 
$count = 1;
foreach($gallery as $gallery_img) :
if($count <= 8) :
?>
        <li class="tile gal_img_<?php echo $count; ?>"  style="background-image: url(<?php echo $gallery_img; ?>)"> <a href="<?php echo str_replace('-300x200','',$gallery_img); ?>" class="gallery_a" rel="gallery1"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bg/bg_gallery_0<?php echo $count; ?>.png"> </a> </li>
        <?	
$count++;
else: ?>
        <a href="<?php echo str_replace('-300x200','',$gallery_img); ?>" class="gallery_a" rel="gallery1"></a>
        <?php endif;
endforeach;
?>
      </ul>
    </div>
  </section>
  <!-- GALLERY  -->
  <section class="page" id="catering">
    <div class="vert-middle-align">
      <div class="container">
        <?php $post = get_post(24); // CATERING PAGE ?>
        <?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
        <h1><?php echo $post->post_title; ?></h1>
        <div class="row content">
          <div class="two-col"><img src="<?php echo $feat_image; ?>"  class="img-responsive"/></div>
          <div class="two-col"><?php echo $post->post_content; ?> </div>
        </div>
      </div>
    </div>
    <footer>
      <div class="container">
        <div class="row">
          <div class="two-col">© barbuto restaurant 2015</div>
          <div class="two-col">Website design by <a href="http://spicybroccoli.com/" target="_blank">spicy broccoli media</a></div>
        </div>
      </div>
    </footer>
  </section>
  <!-- CATERING -->
  <div id="sign" class="page">
    <div class="sign_up" id="sign_up">
      <form name="" action="/#wpcf7-f405-o1" method="post" class="wpcf7-form" novalidate>
        <span class="wpcf7-form-control-wrap contact-name">
        <input type="text" name="contact-name" placeholder="Name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
        </span> <span class="wpcf7-form-control-wrap contact-phone">
        <input type="text" name="contact-phone" placeholder="Phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
        </span>
      </form>
      <span class="wpcf7-form-control-wrap contact-phone">
      <input type="checkbox" name="contact-phone" placeholder="Phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
      I agree to the <a class="box-inline" href="#privacypolicy">privacy policy</a></span> <a class="book-now-btn" href="#">Sign Up</a>
      </form>
      <div id="privacypolicy" style="display:none;">
        <?php
$post_policy = get_post(38, ARRAY_A);
?>
        <h1><?php echo $post_policy['post_title']; ?></h1>
        <p><?php echo $post_policy['post_content']; ?></p>
      </div>
      <div class="sign_up_img vip-club"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/btn_vip.png" width="45" height="223"  alt=""/></div>
    </div>
  </div>
  <!-- SIGN UP --> 
</div>
<!-- MAIN -->
<?php get_footer(); ?>
