//	tiles ##########################
// JavaScript Document
var colCount = 0;
var margin = 0;
var max_cols = 3; //quantit of columns
var win_size = 434;
var windowWidth = 0;
var blocks = [];

jQuery(function(){
	jQuery(window).resize(setupBlocks);
});

function setupBlocks() {
if (jQuery('#gallery_images').length > 0) {
	windowWidth = jQuery(window).width(); 
	
	console.log(windowWidth);
	
	colWidth = jQuery('.tile').outerWidth();

	blocks = [];

	
	colCount = Math.floor(windowWidth/(colWidth+margin*2));
	// previne from empty and bigger columns
	if(colCount == 0){ 
		colCount = 1;
	}else if(colCount > max_cols){
		colCount = max_cols;
	}//endif
	
	
	for(var i=0;i<colCount;i++){
		blocks.push(margin);
	}
	console.log(blocks);
	positionBlocks();
}
}

function positionBlocks() {
	jQuery('.tile').each(function(){
		var min = Array.min(blocks);
		if(min == Infinity) { min = 0; }
		var index = jQuery.inArray(min, blocks);
		var leftPos = margin+(index*(colWidth+margin));
		
		jQuery(this).css({
			'left':leftPos+'px',
			'top':min+'px'
		});
		
		blocks[index] = min+jQuery(this).outerHeight()+margin;
	});	
	var max = Array.max(blocks);
	if(max == Infinity) { max = 0; }
	
	jQuery('.container-tiles').css('height', max);
	jQuery('.container-tiles').css('box-sizing', 'content-box');
}

jQuery(window).load(function () {
	if (jQuery('#container-tiles-home').length > 0) {
	
		jQuery('.tile').each(function(){
			jQuery(this).css('position', 'absolute');
		});
		setupBlocks();
	
	}
});


// Function to get the Min value in Array
Array.min = function(array) {
    return Math.min.apply(Math, array);
};

// Function to get the Min value in Array
Array.max = function(array) {
    return Math.max.apply(Math, array);
};
