$.noConflict();
jQuery( document ).ready(function( $ ) {
  // Code that uses jQuery's $ can follow here.
var spicybroccoli = {};

spicybroccoli.init = function() {
	alert('site working');
}

spicybroccoli.run = function() {
	var me = this;
	return function() {
		me.init();
	}
} 

// JavaScript Document
jQuery('header nav a, a.book-now-btn').click(function(){
	/*console.log(jQuery.attr(this, 'href'));*/
    jQuery('html, body').animate({
        scrollTop: jQuery( jQuery.attr(this, 'href') ).offset().top
    }, 700);
	if(jQuery('header .header-content').hasClass('open')){
		jQuery('header .header-content').removeClass('open');
	}
	return false;
});

//PALMEIRA ACTION
jQuery('nav li.palmeira, .logo-fixed').click(function(){
    jQuery('html, body').animate({
        scrollTop: jQuery( '#home' ).offset().top
    }, 700);
	return false;
});

// vip club
jQuery('.vip-club').click(function(){
	if(jQuery('div#sign_up').hasClass('open')){
		jQuery('div#sign_up').removeClass('open');
	}else{
		jQuery('div#sign_up').addClass('open');
	}
	return false;
});

//	hide and shows barbuto logo WHEN SCROLLLING
jQuery(document).scroll(function(){	
	
	showsLogo();
	
});

function showsLogo ( w_size , h_size ) {
	if (w_size === undefined) {
          w_size = 767;
    }
	if (h_size === undefined) {
          h_size = 569; // bigger than iphone 5 screen
    } 
	var topScroll = jQuery(this).scrollTop(); //returns scroll position from top of document
	if(jQuery( window ).width() < 767){
			if(topScroll > 100 || jQuery( window ).height() < h_size){		
					jQuery('.logo-fixed').css('top', '5px');
			}else if(topScroll < 100){
					jQuery('.logo-fixed').css('top', '-150px');
			}
	}else{
			if(topScroll > 280 || jQuery( window ).height() < h_size){		
					jQuery('.logo-fixed').css('top', '21px');
			}else if(topScroll < 280){
				jQuery('.logo-fixed').css('top', '-150px');
			}
	}
}
// call function
showsLogo();

jQuery('#trigger').click(function(){
	if(jQuery('header .header-content').hasClass('open')){
		jQuery('header .header-content').removeClass('open');
	}else{
		jQuery('header .header-content').addClass('open');
	}
	return false;
});
// FANCYBOX
$(".gallery_a").fancybox({
		prevEffect		: 'none',
		nextEffect		: 'none',
		closeBtn		: false,
		padding			: 4,
		beforeShow: function(){
			$("body").css({'overflow-y':'hidden'});
		},
		afterClose: function(){
 			$("body").css({'overflow-y':'visible'});
		},
		helpers		: {
			title	: { type : 'inside' },
			buttons	: {},
			overlay: {
			  locked: false //do not go back to top
			}
		}
	});
$(".box-inline").fancybox({
		maxWidth	: 666,
		maxHeight	: 550,
		fitToView	: false,
		width		: '100%',
		height		: '100%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		helpers: {
			overlay: {
			  locked: false //do not go back to top
			}
		  }
	});
	$('.box-media').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		helpers : {
			media : {},
			overlay: {
			  locked: false //do not go back to top
			}
		}
	});
// Run it
jQuery(spicybroccoli.run);
});